# conf.py

# -- General configuration ------------------------------------------------

import os
import sys
sys.path.insert(0, os.path.abspath('.'))

project = 'Spring Boot Site'
release = '1.0'

extensions = ['sphinxcontrib.javadocapi']

# Path to the Javadoc output directory
javadoc_output_dir = os.path.abspath('backend/target/site/apidocs')
javadoc_url = 'file://' + javadoc_output_dir
javadoc_root = 'index.html'  # Root file of Javadoc


templates_path = ['_templates']
exclude_patterns = []

# -- Options for HTML output ---------------------------------------------

html_theme = 'alabaster'

# -- Options for LaTeX output --------------------------------------------

latex_documents = [
    ('index', 'YourProjectName.tex', 'Your Project Name Documentation',
     'Your Name', 'manual'),
]
