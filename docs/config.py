# conf.py

# -- General configuration ------------------------------------------------

import os
import sys
sys.path.insert(0, os.path.abspath('.'))

project = 'Your Project Name'
author = 'Your Name'
release = '1.0'

extensions = []

templates_path = ['_templates']
exclude_patterns = []

# -- Options for HTML output ---------------------------------------------

html_theme = 'alabaster'

# -- Options for LaTeX output --------------------------------------------

latex_documents = [
    ('index', 'YourProjectName.tex', 'Your Project Name Documentation',
     'Your Name', 'manual'),
]
